const form = document.getElementById("login-container");
const firstName = document.getElementById("enter-first");
const lastName = document.getElementById("enter-last");
const email = document.getElementById("enter-email");
const password = document.getElementById("enter-pass");
const repeatPassword = document.getElementById("enter-repeat-pass");
const checkBoxStatus = document.getElementById('check-T-C');

document.addEventListener("DOMContentLoaded", function(){
    console.log("CONTENT LOADED.");
})
  
form.addEventListener("submit", function(event){
    event.preventDefault();
    
    if (validateSignUp() === 5) {
        console.log("Entered in IF");
        localStorage.setItem("first-name", firstName.value);
        localStorage.setItem("last-name", lastName.value);
        localStorage.setItem("email", email.value);
        localStorage.setItem("password", password.value);

        if (checkBoxStatus.checked) {
            sendSuccess(checkBoxStatus.parentElement);
            window.location.href = "/";
        }
        else {
            sendError(checkBoxStatus.parentElement, "Kindly tick on Term and conditions to proceed.");
        }
    };
})

const sendError = (element, errorMessage) => {
    const control = element.parentElement;
    const showError = control.querySelector('.error');

    showError.innerText = errorMessage;
    control.classList.add('error');
    control.classList.remove('success');
    
}

const sendSuccess = (element) => {
    const control = element.parentElement;
    const showError = control.querySelector('.error');

    showError.innerText = "";
    control.classList.add('success');
    control.classList.remove('error');
}

const validateSignUp = () => {
    let count = 0;

    const firstNameValue = firstName.value.trim();
    const lastNameValue = lastName.value.trim();
    const emailValue = email.value.trim();
    const passwordValue = password.value.trim();
    const repeatPasswordValue = repeatPassword.value.trim();
    
    if (firstNameValue.length === 0){
        sendError(firstName,"First Name cannot be an empty.")
    }
    else {
        sendSuccess(firstName);
        count++;
    }
    
    if (lastNameValue.length === 0){
        sendError(lastName,"Last Name cannot be an empty.")
    }
    else {
        sendSuccess(lastName);
        count++;
    }
    
    
    if (emailValue.length === 0){
        sendError(email, "Email cannot be an empty.");
    }
    else if (emailValue.indexOf(".") === -1 || emailValue.indexOf("@")=== -1) {
        sendError(email, "Email must be contain '@' and a '.' character.");
    }
    else if (emailValue.length < 6) {
        console.log(email,emailValue);
        sendError(email, "Enter a valid email address of proper length.");
    }
    else {
        const emailValidationRegex = /^([a-z0-9\.-]+)@([a-z0-9-]+)\.([a-z]{2,8})(\.[a-z]{2,10})?$/;
        if (!emailValidationRegex.test(emailValue)) {
            sendError(email, "Please enter a valid email, example :- abc@gmail.com");
        }
        else {
            sendSuccess(email);
            count++;
        }
    }

    if (passwordValue.length < 6){
        sendError(password,"Password should contain minimum 6 characters.")
    }
    else {
        sendSuccess(password);
        count++;
    }

    if (repeatPasswordValue.length < 6){
        sendError(repeatPassword,"Password should contain minimum 6 characters.")
    }
    else if (repeatPasswordValue !== passwordValue){
        sendError(repeatPassword,"Confirmation Password did not match with previous password.")
    }
    else {
        count++;
        sendSuccess(repeatPassword);
        // console.log(count, "Passed");
    }
      
    return count;
}
