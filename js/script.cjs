
fetch("https://fakestoreapi.com/products")
    .then((response) => {
        return response.json();
    })
    .then((jsonData) => {

        
        const allProductContainer = jsonData.map((item) => {
            
            let productInfoContainer = document.createElement("div");
        
            productInfoContainer.setAttribute("class", `product Product-no-${item.id}`);


            // Creates an element and stores data for product's IMAGE, then append to productInfoContainer.
            const productImage = document.createElement('img');
            productImage.setAttribute("class", `image image-for-id-${item.id}`);
            productImage.setAttribute("src", item.image);
            productInfoContainer.appendChild(productImage);
            
            // Creates an element and stores data for product's TITLE, then append to productInfoContainer.
            const productName = document.createElement("h2");
            productName.setAttribute("class", `name name-for-id-${item.id}`);
            productName.innerText = `${item.title}`;
            productInfoContainer.appendChild(productName);
            
            // Creates an element and stores data for product's PRICE, then append to productInfoContainer.
            const productPrice = document.createElement('div');
            productPrice.setAttribute("class", `price price-for-id-${item.id}`);
            productPrice.innerText = `$${item.price}`;
            productInfoContainer.appendChild(productPrice);
            
            
            // Creates an element and stores data for product's DESCRIPTION, then append to productInfoContainer.
            const productDescription = document.createElement('div');
            productDescription.setAttribute("class", `description description-for-id-${item.id}`);
            productDescription.innerText = `${item.description}`;
            productInfoContainer.appendChild(productDescription);
            
            
            // Creates an element and stores data for product's CATEGORY, then append to productInfoContainer.
            const productCategory = document.createElement('div');
            productCategory.setAttribute("class", `category category-for-id-${item.id}`);
            productCategory.innerText = `Category :- ${item.category}`;
            productInfoContainer.appendChild(productCategory);
            
            
            // Creates an element and stores data for product's RATING, then append to productInfoContainer.
            const productRating = document.createElement('div');
            productRating.setAttribute("class", `rating rating-for-id-${item.id}`);
            // let starRating = "<i class='fa fa-star'></i>".repeat(Math.floor(item.rating.rate));
            productRating.innerHTML = `<h1>${item.rating.count} Users rated this product (${item.rating.rate}/5)</h1>`;
            productInfoContainer.appendChild(productRating);

            return productInfoContainer;
        });
  
        const allProductData = document.createElement('div');
        allProductData.setAttribute("class", 'all-products');
        allProductData.append(...allProductContainer);        
        
        console.log(allProductData);

        document.getElementById('product-cards').appendChild(allProductData);
        
        const loadingIcon = document.querySelector(".lds-ring");
        loadingIcon.remove();

    })
    .catch((error) => {
        const cardContainer = document.createElement('div');
        cardContainer.classList.add("server-error");

        if (error.message.includes('Failed to fetch') > -1) {

            console.log(cardContainer);
            console.error(error, "Invalid URL or Server side Error.");
            cardContainer.innerHTML = "<h1>!!! Unable to receive data from the server due to invalid URL or Server side Error.</h1>";
            document.getElementById("product-cards").appendChild(cardContainer);
        }
        else {
            console.error(error, "!!!Unable to fetch data.");
            cardContainer.innerHTML = "<h1>Internal Server Error.</h1>";
            document.getElementById("product-cards").appendChild(cardContainer);
        }
    });


const userName = localStorage.getItem('first-name');
const firstNav = document.getElementById("sign-up");
const secondNav = document.getElementById("log-in");
if (userName) {
    firstNav.innerText = `Welcome ${userName}`;
    firstNav.removeAttribute("href");
    secondNav.innerText = "Log Out";
    secondNav.setAttribute("class", "log-out");
}

const logOutButton = document.querySelector(".log-out");
    if (logOutButton) {
        logOutButton.addEventListener('click', () => {
        // Clear local storage
        window.location.href = '/';
        localStorage.clear();
    });
}